from django.shortcuts import redirect, render, get_object_or_404
from django.views.generic import ListView, DetailView
from django.contrib.auth.decorators import login_required

from recipes.forms import RecipeForm, MeasureForm, FoodItemForm
from recipes.models import Recipe


def create_recipe(request):
    if request.method == "POST" and RecipeForm:
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save()
            return redirect("single_recipe", pk=recipe.pk)

    form = RecipeForm()

    context = {
        "form": form,
    }
    return render(request, "recipes/new.html", context)


def create_measurement(request):
    if request.method == "POST" and MeasureForm:
        form = MeasureForm(request.POST)
        if form.is_valid():
            measure = form.save()
            return redirect("list_recipes")
    form = MeasureForm()
    return render(request, "ingredients/new_measure.html", {"form": measure})


def create_foodItem(request):
    if request.method == "POST" and FoodItemForm:
        form = FoodItemForm(request.POST)
        if form.is_valid():
            food = form.save()
            return redirect("list_recipes")
    form = FoodItemForm()
    return render(request, "ingredients/new_food.html", {"form": food})


def change_recipe(request, pk):
    if Recipe and RecipeForm:
        instance = Recipe.objects.get(pk=pk)
        if request.method == "POST":
            form = RecipeForm(request.POST, instance=instance)
            if form.is_valid():
                form.save()
                return redirect("single_recipe", pk=pk)
        else:
            form = RecipeForm(instance=instance)
    else:
        form = None
    context = {
        "form": form,
    }
    return render(request, "recipes/edit.html", context)


def list_recipes(request):

    return render(
        request,
        "recipes/list.html",
        {"list_recipes": Recipe.objects.all()},
    )


def single_recipe(request, pk):

    return render(
        request,
        "recipes/detail.html",
        {
            "single_recipe": Recipe.objects.get(pk=pk) if Recipe else None,
        },
    )


def delete_recipe(request, pk):
    recipe = Recipe.objects.get(pk=pk)
    if recipe:
        Recipe.objects.delete(pk=recipe.pk)
        return redirect("list_recipes")


# class MarkDown(TemplateView):
#     template_name = "index.html"

#     def get_context_data(self, **kwargs):
#         markdowntext = open(
#             os.path.join(os.path.dirname(__file__), "templates/test.md")
#         ).read()

#         context = super().get_context_data(**kwargs)
#         context["markdowntext"] = markdowntext

#         return context


class RecipeListView(ListView):
    model = Recipe
    context_object_name = "recipes"
    template_name = "recipes/list.html"


class RecipeDetailView(DetailView):
    model = Recipe
    context_object_name = "recipe"
    template_name = "recipes.detail.html"
