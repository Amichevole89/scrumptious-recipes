from django.forms import ModelForm


from recipes.models import Recipe, Measure, FoodItem


class RecipeForm(ModelForm):
    class Meta:
        model = Recipe
        fields = [
            "name",
            "author",
            "description",
            "image",
        ]


class MeasureForm(ModelForm):
    class Meta:
        model = Measure
        fields = [
            "name",
            "abbreviation",
        ]


class FoodItemForm(ModelForm):
    class Meta:
        model = FoodItem
        fields = ["name"]
