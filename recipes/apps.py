from django.apps import AppConfig


class RecipesConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "recipes"


class MarkdownifyConfig(AppConfig):
    name = "markdownify"

    def ready(self):
        import markdownify.checks
