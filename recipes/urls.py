from django.urls import path

from recipes.views import (
    create_recipe,
    change_recipe,
    single_recipe,
    list_recipes,
    delete_recipe,
)

urlpatterns = [
    path("", list_recipes, name="list_recipes"),
    path("<int:pk>/", single_recipe, name="single_recipe"),
    path("new/", create_recipe, name="recipe_new"),
    path("edit/<int:pk>/", change_recipe, name="recipe_edit"),
    path("delete/<int:pk>/", delete_recipe, name="recipe_delete"),
]
