## This project should be functioning

## This is a VIEW FUNCTION version of scrumptious

## Create virtual environment

python -m venv .venv

## MacOS RUN venv

source .venv/bin/activate

## Install dependencies

pip install -r requirements.txt

## migrate

python manage.py migrate

## test need migrations

python manage.py makemigrations

## migrate

python manage.py migrate
